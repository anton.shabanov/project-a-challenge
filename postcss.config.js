module.exports = () => ({
  parser: false,
  plugins: {
    'postcss-import': {},
    'postcss-custom-media': {},
    'postcss-nested': {},
    'postcss-hexrgba': {},
    'postcss-preset-env': {},
    'postcss-inline-svg': {},
    cssnano: {},
  },
});

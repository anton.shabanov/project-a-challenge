import zxcvbn from 'zxcvbn';

// eslint-disable-next-line no-useless-escape
const EMAIL_REGEXP = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
const MIN_PASSWORD_SCORE = 2;

export const required = value => {
  const valid = (value || '').toString().trim().length > 0;
  if (valid) return { error: null };
  return { error: 'This field should be required' };
};

export const email = value => {
  const valid = EMAIL_REGEXP.test(value);
  if (valid) return { error: null };
  return { error: `"${value}" is not correct email address` };
};

export const password = value => {
  const result = zxcvbn(value);
  if (result.score >= MIN_PASSWORD_SCORE) return { error: null, ...result };
  return { error: 'The password provided is not strong one', ...result };
};

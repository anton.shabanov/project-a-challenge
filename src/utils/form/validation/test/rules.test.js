import { required, email, password } from 'src/utils/form/validation/rules';

describe('Utils', () => {
  describe('Form', () => {
    describe('Validation', () => {
      describe('required', () => {
        it('should validate empty values', () => {
          expect(required(null).error).not.toBe(null);
          expect(required(undefined).error).not.toBe(null);
          expect(required('').error).not.toBe(null);
          expect(required(' ').error).not.toBe(null);
        });

        it('should validate non empty values', () => {
          expect(required({}).error).toBe(null);
          expect(required('hello').error).toBe(null);
          expect(required(12).error).toBe(null);
          expect(required('1').error).toBe(null);
        });
      });

      describe('email', () => {
        it('should validate non email values', () => {
          expect(email('not-an@email').error).not.toBe(null);
        });

        it('should validate empty values', () => {
          expect(email('john.doe@gmail.com').error).toBe(null);
        });
      });

      describe('password', () => {
        it('should validate non password values', () => {
          expect(password('password').error).not.toBe(null);
        });

        it('should validate password values', () => {
          expect(password('vErySt0ngPa$$').error).toBe(null);
        });
      });
    });
  });
});

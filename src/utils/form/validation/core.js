const checkRules = (field, rules) =>
  rules.reduce((ruleAcct, rule) => {
    if (ruleAcct.error) return ruleAcct;
    const ruleValid = rule(field);
    const { error, ...data } = ruleValid;
    return { error, data };
  }, {});

export default (obj, schema) => {
  const errors = Object.keys(schema).reduce((acct, key) => {
    const rules = schema[key];
    const field = obj[key];
    return { ...acct, [key]: checkRules(field, rules) };
  }, {});

  const isValid =
    Object.keys(errors).filter(k => !!errors[k].error).length === 0;
  return { errors, isValid };
};

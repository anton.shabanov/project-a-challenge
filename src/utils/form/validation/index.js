import validate from './core';
import { required, email, password } from './rules';

export { required, email, password };
export default validate;

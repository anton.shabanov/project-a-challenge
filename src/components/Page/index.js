import Page from './Page';
import PageTitle from './PageTitle';
import PageContainer from './PageContainer';
import PageContent from './PageContent';

export { Page, PageTitle, PageContainer, PageContent };

import styled from 'astroturf';

const page = styled('main')`
  display: flex;
  align-items: center;
  padding: 15px;

  @media (min-width: 481px) {
    padding: 50px 0;
    min-height: auto;
  }
`;

const pageTitle = styled('h1')`
  font-size: 3.2rem;
  font-weight: var(--ph-font-weight-bold);
  color: var(--dune);
  line-height: 1.1875;
  margin: 0;
  padding: 0;

  &.center {
    text-align: center;
  }

  &.inline {
    display: flex;
    flex-direction: row;
    justify-content: space-between;
  }
`;

const pageContainer = styled('div')`
  box-shadow: 0 2px 4px 0 rgba(0, 0, 0, 0.1);
  background-color: var(--white);
  padding: 20px 25px;
  max-width: var(--grid-width-default);
  margin: 0 auto;
  width: 100%;

  @media (min-width: 481px) {
    padding: 37px 53px;
  }
`;

const pageContent = styled('div')`
  position: relative;

  &.spaced {
    margin-top: 30px;
  }
`;

export default {
  Page: page,
  PageTitle: pageTitle,
  PageContainer: pageContainer,
  PageContent: pageContent,
};

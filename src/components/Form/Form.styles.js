import styled from 'astroturf';

const form = styled('form')`
  width: 100%;
`;

const formSection = styled('div')`
  & + & {
    margin-top: 30px;
  }

  &.spaced {
    margin-top: 49px;
  }
`;

export default {
  Form: form,
  FormSection: formSection,
};

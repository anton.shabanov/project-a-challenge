import styled from 'astroturf';
import StrengthIndicator from 'src/components/StrengthIndicator';

const strengthIndicator = styled(StrengthIndicator)`
  margin-top: 15px;
`;

export default {
  StrengthIndicator: strengthIndicator,
};

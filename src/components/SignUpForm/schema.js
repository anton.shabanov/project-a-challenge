import { email, required, password } from 'src/utils/form/validation';

export default {
  email: [required, email],
  password: [required, password],
};

import React from 'react';
import PropTypes from 'prop-types';
import TextField from 'src/components/TextField';
import Button from 'src/components/Button';
import Form, { FormSection } from 'src/components/Form';
import useValidation from 'src/hooks/form/useValidation';
import schema from './schema';
import Styled from './SingUpForm.styles';

const SignUpForm = ({ onSubmit }) => {
  const [{ errors }, { handleSubmit, handleFieldChange }] = useValidation({
    values: {},
    schema,
    onSubmit,
  });
  const passwordScore = errors.password ? errors.password.data.score : null;
  return (
    <Form onSubmit={handleSubmit}>
      <FormSection>
        <TextField
          label="Email"
          name="email"
          error={errors.email && errors.email.error}
          onChange={handleFieldChange}
        />
      </FormSection>

      <FormSection>
        <TextField
          label="Password"
          type="password"
          name="password"
          error={errors.password && errors.password.error}
          onChange={handleFieldChange}
        />
      </FormSection>

      {passwordScore != null && (
        <Styled.StrengthIndicator score={passwordScore} />
      )}

      <FormSection spaced>
        <Button>Submit</Button>
      </FormSection>
    </Form>
  );
};

SignUpForm.propTypes = {
  onSubmit: PropTypes.func.isRequired,
};

export default React.memo(SignUpForm);

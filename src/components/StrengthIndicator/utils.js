import { GOOD_SCORE, PERFECT_SCORE, STRENGTH } from './constant';

// eslint-disable-next-line import/prefer-default-export
export const scoreRate = score => {
  if (score < GOOD_SCORE) return [STRENGTH.BAD, 'Strength: Bad'];
  if (score >= GOOD_SCORE && score < PERFECT_SCORE)
    return [STRENGTH.GOOD, 'Strength: Good'];
  return [STRENGTH.PERFECT, 'Strength: Perfect'];
};

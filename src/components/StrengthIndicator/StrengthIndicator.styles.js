import styled from 'astroturf';

const indicator = styled('div')`
  position: relative;
`;

const list = styled('div')`
  display: flex;
  flex-direction: row;
`;

const step = styled('div')`
  flex: 1;
  height: 8px;
  background: rgba(#858e99, 0.3);

  &.selected {
    background: var(--red);

    &.yellow {
      background: var(--casablanca);
    }

    &.green {
      background: var(--dark-pastel-green);
    }
  }

  & + & {
    margin-left: 5px;
  }
`;

const message = styled('div')`
  margin-top: 10px;
  color: var(--red);
  font-size: 1.4rem;

  &.yellow {
    color: var(--casablanca);
  }

  &.green {
    color: var(--dark-pastel-green);
  }
`;

export default {
  Indicator: indicator,
  Step: step,
  List: list,
  Message: message,
};

import React from 'react';
import { shallow } from 'enzyme';
import StrengthIndicator from 'src/components/StrengthIndicator';

describe('Components', () => {
  describe('StrengthIndicator', () => {
    it('renders 3 indicators for any score', () => {
      const wrapper = shallow(<StrengthIndicator score={1} />);
      const steps = wrapper.find('step');
      expect(steps.length).toBe(3);
    });

    it('renders 1 selected indicator for bad score', () => {
      const wrapper = shallow(<StrengthIndicator score={1} />);
      const steps = wrapper.find({ selected: true });
      expect(steps.length).toBe(1);
    });

    it('renders 2 selected indicator for good score', () => {
      const wrapper = shallow(<StrengthIndicator score={2} />);
      const steps = wrapper.find({ selected: true });
      expect(steps.length).toBe(2);
    });

    it('renders 3 selected indicator for perfect score', () => {
      const wrapper = shallow(<StrengthIndicator score={20} />);
      const steps = wrapper.find({ selected: true });
      expect(steps.length).toBe(3);
    });
  });
});

export const GOOD_SCORE = 2;
export const PERFECT_SCORE = 4;

export const STRENGTH = Object.freeze({
  BAD: 0,
  GOOD: 1,
  PERFECT: 2,
});

export const STRENGTH_LIST = Object.values(STRENGTH);

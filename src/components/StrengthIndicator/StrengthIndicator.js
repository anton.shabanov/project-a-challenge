import React from 'react';
import PropTypes from 'prop-types';
import { scoreRate } from './utils';
import { STRENGTH_LIST, STRENGTH } from './constant';
import Styled from './StrengthIndicator.styles';

const StrengthIndicator = ({ score, ...rest }) => {
  const [rate, message] = scoreRate(score);
  const isGood = rate === STRENGTH.GOOD;
  const isPerfect = rate === STRENGTH.PERFECT;
  return (
    <Styled.Indicator {...rest}>
      <Styled.List>
        {STRENGTH_LIST.map(i => (
          <Styled.Step
            key={i}
            selected={i <= rate}
            yellow={isGood}
            green={isPerfect}
          />
        ))}
      </Styled.List>

      <Styled.Message yellow={isGood} green={isPerfect}>
        {message}
      </Styled.Message>
    </Styled.Indicator>
  );
};

StrengthIndicator.propTypes = {
  score: PropTypes.number.isRequired,
};

export default React.memo(StrengthIndicator);

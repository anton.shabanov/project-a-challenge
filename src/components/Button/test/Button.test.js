import React from 'react';
import { shallow } from 'enzyme';
import Button from 'src/components/Button';

describe('Components', () => {
  describe('Button', () => {
    const onClickHandler = jest.fn();

    it('renders button with text', () => {
      const title = 'Hello World';
      const wrapper = shallow(<Button>{title}</Button>);
      const text = wrapper
        .find('button')
        .first()
        .text();
      expect(text).toBe(title);
    });

    it('renders button tag', () => {
      const wrapper = shallow(<Button>Hello</Button>);
      expect(wrapper.find('button').length).toBe(1);
    });

    it('should be clickable', () => {
      const wrapper = shallow(<Button onClick={onClickHandler} />);

      wrapper.simulate('click');
      expect(onClickHandler).toHaveBeenCalled();
    });
  });
});

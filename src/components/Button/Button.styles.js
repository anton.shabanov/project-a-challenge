import styled from 'astroturf';

const button = styled('button')`
  border-radius: 3px;
  padding: 15px 30px;
  background-image: linear-gradient(to left, #f26f23, var(--casablanca));
  font-size: 1.4rem;
  font-weight: var(--ph-font-weight-bold);
  letter-spacing: 1px;
  line-height: 1.67;
  color: var(--white);
  border: none;
  text-transform: uppercase;
  outline: none;
  cursor: pointer;

  &:hover {
    opacity: 0.9;
  }

  &:disabled {
    opacity: 0.6;
    cursor: not-allowed;
  }
`;

export default {
  Button: button,
};

import styled from 'astroturf';

const field = styled('div')`
  position: relative;
`;

const fieldLabel = styled('label')`
  font-size: 1.3rem;
  font-weight: var(--ph-font-weight-regular);
  color: var(--blue-grey);
  text-transform: uppercase;
  display: block;
`;

const fieldInput = styled('input')`
  border-radius: 3px;
  border: solid 1px rgba(#979797, 0.2);
  background-color: rgba(#d8d8d8, 0.2);
  padding: 15px 14px;
  outline: none;
  font-size: 1.6rem;
  line-height: 1.25;
  letter-spacing: normal;
  color: var(--charcoal-grey);
  margin-top: 8px;

  &:focus {
    border: solid 1px rgba(#979797, 0.4);
  }

  &.error {
    border: solid 1px var(--vermillion);
  }
`;

const fieldError = styled('div')`
  font-size: 1.3rem;
  line-height: 1.54;
  color: var(--red);
  margin-top: 9px;
  word-wrap: break-word;
`;

export default {
  Field: field,
  FieldLabel: fieldLabel,
  FieldInput: fieldInput,
  FieldError: fieldError,
};

import React from 'react';
import PropTypes from 'prop-types';
import Styled from './TextField.styles';

const TextField = ({ label, error, ...rest }) => (
  <Styled.Field>
    <Styled.FieldLabel>
      {label}
      <Styled.FieldInput type="text" error={!!error} {...rest} />
    </Styled.FieldLabel>
    {error && <Styled.FieldError>{error}</Styled.FieldError>}
  </Styled.Field>
);

TextField.propTypes = {
  label: PropTypes.string.isRequired,
  error: PropTypes.string,
};

TextField.defaultProps = {
  error: null,
};

export default React.memo(TextField);

import React from 'react';
import { shallow } from 'enzyme';
import TextField from 'src/components/TextField';

describe('Components', () => {
  describe('TextField', () => {
    it('renders default search with text input and clear icon inside', () => {
      const wrapper = shallow(<TextField />);
      const input = wrapper.find({ type: 'text' });

      expect(input.length).toBe(1);
    });

    it('supports onChange handler', () => {
      const event = { target: { value: 'Hello' } };
      const onChangeHandler = jest.fn();
      const wrapper = shallow(<TextField onChange={onChangeHandler} />);
      const input = wrapper.find({ type: 'text' });

      input.simulate('change', event);

      expect(onChangeHandler).toHaveBeenCalled();
    });
  });
});

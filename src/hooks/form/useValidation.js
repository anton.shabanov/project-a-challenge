import { useState, useCallback } from 'react';
import validate from 'src/utils/form/validation';

export default ({ values = {}, schema, onSubmit }) => {
  const [form, setForm] = useState(values);
  const [errors, setErrors] = useState({});

  const handleFormChange = useCallback(newForm => {
    const { errors: formErrors, isValid } = validate(newForm, schema);
    setErrors(formErrors);
    setForm(newForm);
    return isValid;
  }, []);

  const handleFieldChange = useCallback(
    ({ target }) => {
      const { value } = target;
      const field = target.getAttribute('name');
      const newForm = { ...form, [field]: value };
      handleFormChange(newForm);
    },
    [form],
  );

  const handleSubmit = useCallback(
    event => {
      event.preventDefault();
      const isValid = handleFormChange(form);

      if (isValid) {
        onSubmit(form);
      }
    },
    [form, errors],
  );

  return [{ form, errors }, { handleSubmit, handleFieldChange }];
};

import React, { useCallback } from 'react';
import ErrorBoundary from 'src/components/ErrorBoundary';
import {
  Page,
  PageTitle,
  PageContainer,
  PageContent,
} from 'src/components/Page';
import SignUpForm from 'src/components/SignUpForm';
import logger from 'src/utils/logger';

const SignUpPage = () => {
  const handleFormSubmit = useCallback(form => {
    alert(JSON.stringify(form));
    logger.info(form);
  }, []);

  return (
    <ErrorBoundary>
      <Page>
        <PageContainer>
          <PageTitle inline>Sign Up</PageTitle>
          <PageContent spaced>
            <SignUpForm onSubmit={handleFormSubmit} />
          </PageContent>
        </PageContainer>
      </Page>
    </ErrorBoundary>
  );
};

export default React.memo(SignUpPage);

# Project A Frontend App

## Video Example

Please follow this link to see the app in action: https://monosnap.com/file/ZLKu0rPPh61ncbY3e7slrAjKIMSAkA

## Installation

Make sure you're running node 11.12.0 or above.

```
$ cd project-dir
$ git clone repo-url .
$ yarn # to install packages
$ yarn dev # to start dev server
$ yarn prod # to build production ready html, bundle & assets
```

## Tests, Linters, Prettier

By default tests, linter & prettier are included inside the pre-commit hook, but if you want to run them manually please use these commands:

```
$ yarn pretty # to run prettier
$ yarn lint --fix # to run linter
$ yarn test # to run tests
$ yarn stylelint # to run styles linter
```
